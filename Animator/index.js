"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var index_1 = require("../index");
/**
 * 타임라인의 키 프레임을 나타내는 클래스
 *
 * 애니메이터는 현재 키 프레임과 다음 키 프레임을 참고하여 보간값을 결정한다.
 *
 * @template T 보간 대상 자료형
 */
var KeyFrame = (function () {
    /**
     * 새 키 프레임을 생성한다.
     *
     * @param time 시점(밀리초)
     * @param data 보간 대상 자료
     */
    function KeyFrame(time, data) {
        this.frame = Math.floor(time / 1000 * Animator.fps);
        this.time = time;
        this.data = data;
    }
    KeyFrame.lerpNumber = function (s, t, p) {
        return s + (t - s) * p;
    };
    KeyFrame.lerpString = function (s, t, p) {
        var _a = [s, t].map(function (v) { return v.match(KeyFrame.REGEXP_SUFFIX); }), sMatch = _a[0], tMatch = _a[1];
        var _b = [sMatch[1], tMatch[1]].map(Number), sValue = _b[0], tValue = _b[1];
        var _c = [sMatch[2], tMatch[2]], sSuffix = _c[0], tSuffix = _c[1];
        if (sSuffix !== tSuffix) {
            throw Error("Insufficient suffix (" + sSuffix + ", " + tSuffix + ")");
        }
        return KeyFrame.lerpNumber(sValue, tValue, p) + sSuffix;
    };
    KeyFrame.lerpObject = function (s, t, p) {
        var R;
        if (typeof s === "number" && typeof t === "number") {
            R = KeyFrame.lerpNumber(s, t, p);
        }
        else if (typeof s === "string" && typeof t === "string") {
            R = KeyFrame.lerpString(s, t, p);
        }
        else if (typeof s === "object" && typeof t === "object") {
            R = {};
            for (var i in s) {
                if (t.hasOwnProperty(i)) {
                    R[i] = KeyFrame.lerpObject(s[i], t[i], p);
                }
                else {
                    R[i] = s[i];
                }
            }
        }
        return R;
    };
    /**
     * 현재 키 프레임과 목적 키 프레임을 참고해 주어진 시점에서 보간을 수행한 결과를 반환한다.
     *
     * @param destKF 목적 키 프레임
     * @param time 진행 시간
     */
    KeyFrame.prototype.lerp = function (destKF, time) {
        var p = (time - this.time) / (destKF.time - this.time);
        return KeyFrame.lerpObject(this.data, destKF.data, p);
    };
    /**
     * 이벤트 핸들러를 설정한다. 이벤트 핸들러는 각 이벤트별로 하나씩만 설정할 수 있다.
     *
     * @param key 이벤트 식별자
     * @param callback 이벤트 핸들러
     */
    KeyFrame.prototype["with"] = function (key, callback) {
        switch (key) {
            case "enter":
                this.onEnter = callback;
                break;
            case "exit":
                this.onExit = callback;
                break;
        }
        return this;
    };
    KeyFrame.REGEXP_SUFFIX = /^(-?\d+(?:\.\d*)?)([^\d].*)$/;
    return KeyFrame;
}());
exports.KeyFrame = KeyFrame;
/**
 * React 객체의 애니메이션을 전담하는 클래스
 *
 * 자식으로 함수가 들어가야 하며 해당 함수의 첫 인자로 현재 애니메이션 정보를 얻을 수 있다.
 */
var Animator = (function (_super) {
    __extends(Animator, _super);
    function Animator(props) {
        var _this = _super.call(this) || this;
        _this.timer = -1;
        _this.state = {
            playing: false,
            frame: 0,
            time: 0,
            percent: 0,
            timeline: props.timeline || [],
            data: props.timeline ? props.timeline[0].data : null
        };
        _this.onTick = _this.onTick.bind(_this);
        return _this;
    }
    /**
     * 주어진 보간 객체 자료 구조를 이용하는 익명 애니메이터 클래스를 반환한다.
     *
     * @template T 보간 객체의 자료형
     * @template U 외부 객체의 자료형
     */
    Animator.createCustomAnimator = function () {
        return (function (_super) {
            __extends(_CustomAnimator, _super);
            function _CustomAnimator() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            return _CustomAnimator;
        }(Animator));
    };
    Animator.prototype.componentDidMount = function () {
        if (this.props.startOnMount)
            this.start();
    };
    /*componentDidUpdate(pp:AnimatorProps<T, U>, ps:AnimatorState<T, U>):void{
        if(this.props.updateWhenExternalChanged && this.state.external !== ps.external){
            this.start();
        }
    }*/
    Animator.prototype.render = function () {
        if (this.props.hideWhenDisabled && !this.state.playing)
            return null;
        var R = this.props.children.call(this, this.state);
        R.props.className = "anim " + (R.props.className || "");
        return R;
    };
    Animator.prototype.onTick = function () {
        var time = this.inState('time').add(1000 / Animator.fps);
        var keyFrame;
        var data;
        keyFrame = this.state.timeline[this.currentKFIndex];
        if (time >= this.nextKFInterval) {
            if (keyFrame.onExit)
                keyFrame.onExit();
            keyFrame = this.state.timeline[++this.currentKFIndex];
            this.nextKFInterval = this.getNextKFInterval();
            if (keyFrame.onEnter)
                keyFrame.onEnter();
        }
        if (this.nextKFInterval === Infinity) {
            data = keyFrame.data;
        }
        else {
            data = keyFrame.lerp(this.state.timeline[this.currentKFIndex + 1], time);
        }
        if (time >= this.interval) {
            time = this.inState('time').set(this.interval);
            if (this.props.onFinish)
                this.props.onFinish();
            this.stop();
        }
        this.inState('frame').add(1);
        this.inState('percent').set(time / this.interval);
        this.inState('data').set(data);
    };
    Animator.prototype.getNextKFInterval = function () {
        if (this.state.timeline.length <= this.currentKFIndex + 1)
            return Infinity;
        return this.state.timeline[this.currentKFIndex + 1].time;
    };
    /**
     * 타임라인에 나열된 키 프레임을 하나 골라 주어진 키 프레임으로 대체한다.
     *
     * @param index 키 프레임 번호
     * @param value 새 키 프레임
     */
    Animator.prototype.setKeyFrame = function (index, value) {
        return this.inState('timeline').splice(index, 1, value);
    };
    /**
     * 애니메이션을 시작 지점부터 재생한다.
     */
    Animator.prototype.start = function () {
        var keyFrame = this.state.timeline[this.currentKFIndex = 0];
        if (keyFrame.onEnter)
            keyFrame.onEnter();
        this.nextKFInterval = this.getNextKFInterval();
        this.interval = this.state.timeline[this.state.timeline.length - 1].time;
        if (this.timer === -1) {
            this.timer = window.setInterval(this.onTick, 1000 / Animator.fps);
        }
        this.setState({
            playing: true,
            time: 0,
            frame: 0
        });
    };
    /**
     * 애니메이션을 중단한다.
     */
    Animator.prototype.stop = function () {
        this.inState('playing').set(false);
        window.clearInterval(this.timer);
        this.timer = -1;
    };
    /**
     * 초당 업데이트 횟수
     */
    Animator.fps = 30;
    return Animator;
}(index_1["default"]));
exports.Animator = Animator;
/**
 * CSS 속성만을 수정하는 애니메이터 클래스
 */
var CSSAnimator = (function (_super) {
    __extends(CSSAnimator, _super);
    function CSSAnimator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 현재 보간된 CSS 속성 정보로부터 원하는 속성만을 골라 재구성한 CSS 속성 객체를 반환한다.
     *
     * @param keys 원하는 속성 배열
     */
    CSSAnimator.prototype.filter = function () {
        var keys = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            keys[_i] = arguments[_i];
        }
        var R = {};
        for (var _a = 0, keys_1 = keys; _a < keys_1.length; _a++) {
            var v = keys_1[_a];
            R[v] = this.state.data[v];
        }
        return R;
    };
    return CSSAnimator;
}(Animator));
exports.CSSAnimator = CSSAnimator;
//# sourceMappingURL=index.js.map