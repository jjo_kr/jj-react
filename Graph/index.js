"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var jj_format_1 = require("jj-format");
var index_1 = require("../index");
/**
 * 주어진 값을 막대 그래프로 표현하는 컴포넌트
 */
var Graph = (function (_super) {
    __extends(Graph, _super);
    function Graph() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Graph.prototype.render = function () {
        var rate = jj_format_1["default"].ofRate(this.props.value - (this.props.minimum || 0), (this.props.maximum || 100) - (this.props.minimum || 0));
        var hint = this.props.hint
            ? React.createElement("div", { className: "jj-graph-hint" }, this.props.hint(this.props.minimum, this.props.value, this.props.maximum))
            : null;
        return React.createElement("div", { className: "jj-graph " + (this.props.className || "") },
            React.createElement("div", { className: "jj-graph-bar", style: (_a = {},
                    _a[this.props.isVertical ? 'height' : 'width'] = rate,
                    _a) }),
            hint,
            this.props.children);
        var _a;
    };
    return Graph;
}(index_1["default"]));
exports.Graph = Graph;
//# sourceMappingURL=index.js.map