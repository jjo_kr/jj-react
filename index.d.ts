declare module 'jj-react' {
	/// <reference types="react" />
	import React = require("react");
	/**
	 * 편리한 리액트 컴포넌트 클래스
	 *
	 * 다루기 어려운 상태(state) 객체를 보다 쉽게 수정할 수 있도록 설계되었다.
	 *
	 * @template P 리액트 속성의 자료형
	 * @template S 리액트 상태의 자료형
	 */
	export default class JRComponent<P = {}, S = {}> extends React.Component<P, S> {
	    protected constructor(props?: P);
	    /**
	     * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
	     *
	     * @param key 상태 식별자
	     */
	    inState<K extends keyof S>(key: K): JRState<P, S, K>;
	}
	/**
	 * 편리한 리액트 순수 컴포넌트 클래스
	 *
	 * @template P 리액트 속성의 자료형
	 * @template S 리액트 상태의 자료형
	 */
	export class JRPureComponent<P = {}, S = {}> extends React.PureComponent<P, S> {
	    protected constructor(props?: P);
	    /**
	     * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
	     *
	     * @param key 상태 식별자
	     */
	    inState<K extends keyof S>(key: K): JRState<P, S, K>;
	}
	/**
	 * 리액트 컴포넌트의 상태 객체의 한 값을 다루는 클래스
	 *
	 * 메소드 {@link JRComponent#inState}로부터 인스턴스를 얻어 상태를 수정할 수 있도록 한다.
	 */
	export class JRState<P, S, K extends keyof S> {
	    private caller;
	    private key;
	    constructor(caller: JRComponent<P, S>, key: keyof S);
	    /**
	     * 대상 값에 주어진 값을 추가시키고 그 결과를 반환한다.
	     *
	     * 대상 값은 숫자 또는 문자열이어야 한다.
	     *
	     * @param delta 증가량. 다루는 값이 숫자인 경우 이 값을 수치로써 더하고, 문자열인 경우 이 값을 가장 끝에 더한다.
	     */
	    add(delta: S[K]): S[K];
	    /**
	     * 대상 값에 주어진 값을 가장 끝에 추가시킨다.
	     *
	     * 대상 값은 배열이어야 한다.
	     *
	     * @param items 추가 항목
	     */
	    push<T>(...items: T[]): Promise<void>;
	    /**
	     * 대상 값을 주어진 인자의 내용에 맞도록 삭제 및 추가시킨다.
	     *
	     * 대상 값은 배열이어야 한다.
	     *
	     * @param index 축이 되는 배열의 인덱스
	     * @param deletes 축에서부터 삭제시킬 항목의 수
	     * @param items 삭제 후 추가시킬 항목
	     */
	    splice<T>(index: number, deletes: number, ...items: T[]): Promise<void>;
	    /**
	     * 대상 값을 주어진 값으로 대체하고 그 결과를 반환한다.
	     *
	     * @param value 새로운 값
	     */
	    set(value: S[K]): S[K];
	}

}
declare module 'jj-react/Animator' {
	/// <reference types="react" />
	import React = require("react");
	import JRComponent from 'jj-react';
	/**
	 * 애니메이터가 사용하는 React 속성 자료 구조
	 *
	 * @template T 보간 객체의 자료형
	 * @template U 외부 객체의 자료형
	 */
	export type AnimatorProps<T, U> = {
	    /**
	     * 자식 반환 함수
	     *
	     * 보간 정보를 담은 상태 객체를 인자로 받아 자식 객체를 반환하는 함수이다.
	     */
	    'children': (state: AnimatorState<T, U>) => JSX.Element;
	    /**
	     * 타임라인
	     *
	     * 키 프레임의 배열이다. 키 프레임은 시간 순으로 정렬되어야 하며
	     * 애니메이션이 시작되었을 때 앞에서부터 하나씩 보간을 위한 지표로 삼는다.
	     *
	     * 최소 두 개의 키 프레임이 있어야 애니메이션이 작동한다.
	     */
	    'timeline'?: KeyFrame<T>[];
	    /**
	     * React 마운트 시 애니메이션 자동 시작 여부
	     */
	    'startOnMount'?: boolean;
	    /**
	     * 애니메이션이 종료된 후 컴포넌트 숨기기 여부
	     */
	    'hideWhenDisabled'?: boolean;
	    /**
	     * 애니메이션이 종료된 후 호출되는 콜백 함수
	     */
	    'onFinish'?: () => void;
	};
	/**
	 * 애니메이터가 사용하는 React 상태 자료 구조
	 *
	 * @template T 보간 객체의 자료형
	 * @template U 외부 객체의 자료형
	 */
	export type AnimatorState<T, U> = {
	    /**
	     * 현재 애니메이션의 동작 여부
	     */
	    'playing': boolean;
	    /**
	     * 현재 애니메이션의 프레임 번호
	     *
	     * 키 프레임과는 무관하다.
	     */
	    'frame': number;
	    /**
	     * 현재 애니메이션의 경과 시간
	     */
	    'time': number;
	    /**
	     * 현재 애니메이션의 경과 정도
	     *
	     * 현재 경과 시간을 총 소요 시간으로 나눈 값이며 0부터 1 사이의 값을 가진다.
	     */
	    'percent': number;
	    /**
	     * 타임라인
	     *
	     * @see {AnimatorProps.timeline}
	     */
	    'timeline': KeyFrame<T>[];
	    /**
	     * 보간 객체
	     *
	     * 현재 경과 시간과 두 키 프레임의 정보를 이용해 구한 값이다.
	     */
	    'data': T;
	    /**
	     * 외부 객체
	     *
	     * 외부에서 애니메이터의 자식을 편집할 때 보간 객체 이외의 참조가 필요한 경우
	     * 이 객체를 이용할 수 있다.
	     */
	    'external'?: U;
	};
	/**
	 * 타임라인의 키 프레임을 나타내는 클래스
	 *
	 * 애니메이터는 현재 키 프레임과 다음 키 프레임을 참고하여 보간값을 결정한다.
	 *
	 * @template T 보간 대상 자료형
	 */
	export class KeyFrame<T> {
	    private static REGEXP_SUFFIX;
	    private static lerpNumber(s, t, p);
	    private static lerpString(s, t, p);
	    private static lerpObject(s, t, p);
	    /**
	     * 현재 프레임. 키 프레임과는 무관하다.
	     */
	    readonly frame: number;
	    /**
	     * 현재 진행 시간
	     */
	    readonly time: number;
	    /**
	     * 보간 대상 자료
	     */
	    readonly data: T;
	    /**
	     * 애니메이션이 이 프레임을 참조하기 시작했을 때 호출되는 함수
	     */
	    onEnter: Function;
	    /**
	     * 애니메이션이 이 프레임을 더 이상 참조하지 않을 때 호출되는 함수
	     */
	    onExit: Function;
	    /**
	     * 새 키 프레임을 생성한다.
	     *
	     * @param time 시점(밀리초)
	     * @param data 보간 대상 자료
	     */
	    constructor(time: number, data: T);
	    /**
	     * 현재 키 프레임과 목적 키 프레임을 참고해 주어진 시점에서 보간을 수행한 결과를 반환한다.
	     *
	     * @param destKF 목적 키 프레임
	     * @param time 진행 시간
	     */
	    lerp(destKF: KeyFrame<T>, time: number): T;
	    /**
	     * 이벤트 핸들러를 설정한다. 이벤트 핸들러는 각 이벤트별로 하나씩만 설정할 수 있다.
	     *
	     * @param key 이벤트 식별자
	     * @param callback 이벤트 핸들러
	     */
	    with(key: "enter" | "exit", callback: Function): this;
	}
	/**
	 * React 객체의 애니메이션을 전담하는 클래스
	 *
	 * 자식으로 함수가 들어가야 하며 해당 함수의 첫 인자로 현재 애니메이션 정보를 얻을 수 있다.
	 */
	export class Animator<T = any, U = any> extends JRComponent<AnimatorProps<T, U>, AnimatorState<T, U>> {
	    /**
	     * 초당 업데이트 횟수
	     */
	    static fps: number;
	    /**
	     * 주어진 보간 객체 자료 구조를 이용하는 익명 애니메이터 클래스를 반환한다.
	     *
	     * @template T 보간 객체의 자료형
	     * @template U 외부 객체의 자료형
	     */
	    static createCustomAnimator<T = any, U = any>(): new () => Animator<T, U>;
	    private timer;
	    private currentKFIndex;
	    private nextKFInterval;
	    private interval;
	    constructor(props: AnimatorProps<T, U>);
	    componentDidMount(): void;
	    render(): React.ReactNode;
	    private onTick();
	    private getNextKFInterval();
	    /**
	     * 타임라인에 나열된 키 프레임을 하나 골라 주어진 키 프레임으로 대체한다.
	     *
	     * @param index 키 프레임 번호
	     * @param value 새 키 프레임
	     */
	    setKeyFrame(index: number, value: KeyFrame<T>): Promise<void>;
	    /**
	     * 애니메이션을 시작 지점부터 재생한다.
	     */
	    start(): void;
	    /**
	     * 애니메이션을 중단한다.
	     */
	    stop(): void;
	}
	/**
	 * CSS 속성만을 수정하는 애니메이터 클래스
	 */
	export class CSSAnimator<U = any> extends Animator<React.CSSProperties, U> {
	    /**
	     * 현재 보간된 CSS 속성 정보로부터 원하는 속성만을 골라 재구성한 CSS 속성 객체를 반환한다.
	     *
	     * @param keys 원하는 속성 배열
	     */
	    filter(...keys: (keyof React.CSSProperties)[]): React.CSSProperties;
	}

}
declare module 'jj-react/Graph' {
	/// <reference types="react" />
	import JRComponent from 'jj-react';
	/**
	 * 그래프 컴포넌트의 React 속성 자료 구조
	 */
	export type GraphProps = {
	    /**
	     * 추가 클래스 이름
	     *
	     * 그래프 컴포넌트에는 클래스 `jj-graph`가 기본적으로 붙고,
	     * 그 뒤에 이 속성의 값이 추가된다.
	     */
	    'className'?: string;
	    /**
	     * 그래프 값의 최소 수치
	     */
	    'minimum'?: number;
	    /**
	     * 그래프 값의 최대 수치
	     */
	    'maximum'?: number;
	    /**
	     * 그래프 값
	     */
	    'value': number;
	    /**
	     * 컴포넌트 위에 보일 힌트 문구를 반환하는 함수
	     */
	    'hint'?: (min: number, v: number, max: number) => string;
	    /**
	     * 너비 대신 높이로 값을 시각화할지 여부
	     */
	    'isVertical'?: boolean;
	};
	/**
	 * 주어진 값을 막대 그래프로 표현하는 컴포넌트
	 */
	export class Graph extends JRComponent<GraphProps> {
	    render(): JSX.Element;
	}

}
