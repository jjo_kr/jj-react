"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
var ReactStateUpdater = require("immutability-helper");
/**
 * 편리한 리액트 컴포넌트 클래스
 *
 * 다루기 어려운 상태(state) 객체를 보다 쉽게 수정할 수 있도록 설계되었다.
 *
 * @template P 리액트 속성의 자료형
 * @template S 리액트 상태의 자료형
 */
var JRComponent = (function (_super) {
    __extends(JRComponent, _super);
    function JRComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    /**
     * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
     *
     * @param key 상태 식별자
     */
    JRComponent.prototype.inState = function (key) {
        return new JRState(this, key);
    };
    return JRComponent;
}(React.Component));
exports["default"] = JRComponent;
/**
 * 편리한 리액트 순수 컴포넌트 클래스
 *
 * @template P 리액트 속성의 자료형
 * @template S 리액트 상태의 자료형
 */
var JRPureComponent = (function (_super) {
    __extends(JRPureComponent, _super);
    function JRPureComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    /**
     * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
     *
     * @param key 상태 식별자
     */
    JRPureComponent.prototype.inState = function (key) {
        return new JRState(this, key);
    };
    return JRPureComponent;
}(React.PureComponent));
exports.JRPureComponent = JRPureComponent;
/**
 * 리액트 컴포넌트의 상태 객체의 한 값을 다루는 클래스
 *
 * 메소드 {@link JRComponent#inState}로부터 인스턴스를 얻어 상태를 수정할 수 있도록 한다.
 */
var JRState = (function () {
    function JRState(caller, key) {
        this.caller = caller;
        this.key = key;
    }
    /**
     * 대상 값에 주어진 값을 추가시키고 그 결과를 반환한다.
     *
     * 대상 값은 숫자 또는 문자열이어야 한다.
     *
     * @param delta 증가량. 다루는 값이 숫자인 경우 이 값을 수치로써 더하고, 문자열인 경우 이 값을 가장 끝에 더한다.
     */
    JRState.prototype.add = function (delta) {
        var origin = this.caller.state[this.key];
        var result;
        if (typeof origin === "number")
            result = (_a = {}, _a[this.key] = origin + Number(delta), _a);
        else if (typeof origin === "string")
            result = (_b = {}, _b[this.key] = origin + String(delta), _b);
        else
            throw Error("Invalid state update: " + delta + " onto the key " + this.key);
        this.caller.setState(result);
        return result[this.key];
        var _a, _b;
    };
    /**
     * 대상 값에 주어진 값을 가장 끝에 추가시킨다.
     *
     * 대상 값은 배열이어야 한다.
     *
     * @param items 추가 항목
     */
    JRState.prototype.push = function () {
        var _this = this;
        var items = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            items[_i] = arguments[_i];
        }
        return new Promise(function (res, rej) {
            _this.caller.setState(function (prev, p) {
                var origin = prev[_this.key];
                if (!(origin instanceof Array))
                    throw Error("Invalid state update: " + items + " onto the key " + _this.key);
                return _a = {},
                    _a[_this.key] = ReactStateUpdater(origin, {
                        $push: items
                    }),
                    _a;
                var _a;
            }, res);
        });
    };
    /**
     * 대상 값을 주어진 인자의 내용에 맞도록 삭제 및 추가시킨다.
     *
     * 대상 값은 배열이어야 한다.
     *
     * @param index 축이 되는 배열의 인덱스
     * @param deletes 축에서부터 삭제시킬 항목의 수
     * @param items 삭제 후 추가시킬 항목
     */
    JRState.prototype.splice = function (index, deletes) {
        var _this = this;
        var items = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            items[_i - 2] = arguments[_i];
        }
        return new Promise(function (res, rej) {
            _this.caller.setState(function (prev, p) {
                var origin = prev[_this.key];
                if (!(origin instanceof Array))
                    throw Error("Invalid state update: " + items + " onto the key " + _this.key);
                return _a = {},
                    _a[_this.key] = ReactStateUpdater(origin, {
                        $splice: [[index, deletes].concat(items)]
                    }),
                    _a;
                var _a;
            }, res);
        });
    };
    /**
     * 대상 값을 주어진 값으로 대체하고 그 결과를 반환한다.
     *
     * @param value 새로운 값
     */
    JRState.prototype.set = function (value) {
        var _this = this;
        this.caller.setState(function (prev, p) {
            return (_a = {},
                _a[_this.key] = value,
                _a);
            var _a;
        });
        return value;
    };
    return JRState;
}());
exports.JRState = JRState;
//# sourceMappingURL=index.js.map