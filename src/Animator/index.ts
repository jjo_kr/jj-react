import React = require("react");
import JRComponent from "../index";

/**
 * 애니메이터가 사용하는 React 속성 자료 구조
 * 
 * @template T 보간 객체의 자료형
 * @template U 외부 객체의 자료형
 */
export type AnimatorProps<T, U> = {
	/**
	 * 자식 반환 함수
	 * 
	 * 보간 정보를 담은 상태 객체를 인자로 받아 자식 객체를 반환하는 함수이다.
	 */
	'children': (state:AnimatorState<T, U>) => JSX.Element,
	/**
	 * 타임라인
	 * 
	 * 키 프레임의 배열이다. 키 프레임은 시간 순으로 정렬되어야 하며
	 * 애니메이션이 시작되었을 때 앞에서부터 하나씩 보간을 위한 지표로 삼는다.
	 * 
	 * 최소 두 개의 키 프레임이 있어야 애니메이션이 작동한다.
	 */
	'timeline'?: KeyFrame<T>[],
	/**
	 * React 마운트 시 애니메이션 자동 시작 여부
	 */
	'startOnMount'?: boolean,
	/**
	 * 애니메이션이 종료된 후 컴포넌트 숨기기 여부
	 */
	'hideWhenDisabled'?: boolean,
	/**
	 * 애니메이션이 종료된 후 호출되는 콜백 함수
	 */
	'onFinish'?: () => void
};
/**
 * 애니메이터가 사용하는 React 상태 자료 구조
 * 
 * @template T 보간 객체의 자료형
 * @template U 외부 객체의 자료형
 */
export type AnimatorState<T, U> = {
	/**
	 * 현재 애니메이션의 동작 여부
	 */
	'playing': boolean,
	/**
	 * 현재 애니메이션의 프레임 번호
	 * 
	 * 키 프레임과는 무관하다.
	 */
	'frame': number,
	/**
	 * 현재 애니메이션의 경과 시간
	 */
	'time': number,
	/**
	 * 현재 애니메이션의 경과 정도
	 * 
	 * 현재 경과 시간을 총 소요 시간으로 나눈 값이며 0부터 1 사이의 값을 가진다.
	 */
	'percent': number,
	/**
	 * 타임라인
	 * 
	 * @see {AnimatorProps.timeline}
	 */
	'timeline': KeyFrame<T>[],
	/**
	 * 보간 객체
	 * 
	 * 현재 경과 시간과 두 키 프레임의 정보를 이용해 구한 값이다.
	 */
	'data': T,
	/**
	 * 외부 객체
	 * 
	 * 외부에서 애니메이터의 자식을 편집할 때 보간 객체 이외의 참조가 필요한 경우
	 * 이 객체를 이용할 수 있다.
	 */
	'external'?: U
};

/**
 * 타임라인의 키 프레임을 나타내는 클래스
 * 
 * 애니메이터는 현재 키 프레임과 다음 키 프레임을 참고하여 보간값을 결정한다.
 * 
 * @template T 보간 대상 자료형
 */
export class KeyFrame<T>{
	private static REGEXP_SUFFIX:RegExp = /^(-?\d+(?:\.\d*)?)([^\d].*)$/;

	private static lerpNumber(s:number, t:number, p:number):number{
		return s + (t - s) * p;
	}
	private static lerpString(s:string, t:string, p:number):string{
		let [ sMatch, tMatch ]:RegExpMatchArray[] = [ s, t ].map(v => v.match(KeyFrame.REGEXP_SUFFIX));
		let [ sValue, tValue ]:number[] = [ sMatch[1], tMatch[1] ].map(Number);
		let [ sSuffix, tSuffix ]:string[] = [ sMatch[2], tMatch[2] ];

		if(sSuffix !== tSuffix){
			throw Error(`Insufficient suffix (${sSuffix}, ${tSuffix})`);
		}
		return KeyFrame.lerpNumber(sValue, tValue, p) + sSuffix;
	}
	private static lerpObject(s:any, t:any, p:number):any{
		let R:any;

		if(typeof s === "number" && typeof t === "number"){
			R = KeyFrame.lerpNumber(s, t, p);
		}else if(typeof s === "string" && typeof t === "string"){
			R = KeyFrame.lerpString(s, t, p);
		}else if(typeof s === "object" && typeof t === "object"){
			R = {};
			for(const i in s){
				if(t.hasOwnProperty(i)){
					R[i] = KeyFrame.lerpObject(s[i], t[i], p);
				}else{
					R[i] = s[i];
				}
			}
		}
		return R;
	}

	/**
	 * 현재 프레임. 키 프레임과는 무관하다.
	 */
	public readonly frame:number;
	/**
	 * 현재 진행 시간
	 */
	public readonly time:number;
	/**
	 * 보간 대상 자료
	 */
	public readonly data:T;
	/**
	 * 애니메이션이 이 프레임을 참조하기 시작했을 때 호출되는 함수
	 */
	public onEnter:Function;
	/**
	 * 애니메이션이 이 프레임을 더 이상 참조하지 않을 때 호출되는 함수
	 */
	public onExit:Function;

	/**
	 * 새 키 프레임을 생성한다.
	 * 
	 * @param time 시점(밀리초)
	 * @param data 보간 대상 자료
	 */
	constructor(time:number, data:T){
		this.frame = Math.floor(time / 1000 * Animator.fps);
		this.time = time;
		this.data = data;
	}
	/**
	 * 현재 키 프레임과 목적 키 프레임을 참고해 주어진 시점에서 보간을 수행한 결과를 반환한다.
	 * 
	 * @param destKF 목적 키 프레임
	 * @param time 진행 시간
	 */
	public lerp(destKF:KeyFrame<T>, time:number):T{
		let p:number = (time - this.time) / (destKF.time - this.time);
		
		return KeyFrame.lerpObject(this.data, destKF.data, p);
	}
	/**
	 * 이벤트 핸들러를 설정한다. 이벤트 핸들러는 각 이벤트별로 하나씩만 설정할 수 있다.
	 * 
	 * @param key 이벤트 식별자
	 * @param callback 이벤트 핸들러
	 */
	public with(key:"enter"|"exit", callback:Function):this{
		switch(key){
			case "enter": this.onEnter = callback; break;
			case "exit": this.onExit = callback; break;
		}
		return this;
	}
}
/**
 * React 객체의 애니메이션을 전담하는 클래스
 * 
 * 자식으로 함수가 들어가야 하며 해당 함수의 첫 인자로 현재 애니메이션 정보를 얻을 수 있다.
 */
export class Animator<T = any, U = any> extends JRComponent<AnimatorProps<T, U>, AnimatorState<T, U>>{
	/**
	 * 초당 업데이트 횟수
	 */
	public static fps:number = 30;

	/**
	 * 주어진 보간 객체 자료 구조를 이용하는 익명 애니메이터 클래스를 반환한다.
	 * 
	 * @template T 보간 객체의 자료형
	 * @template U 외부 객체의 자료형
	 */
	public static createCustomAnimator<T = any, U = any>():new() => Animator<T, U>{
		return class _CustomAnimator extends Animator<T, U>{} as any;
	}

	private timer:number = -1;
	private currentKFIndex:number;
	private nextKFInterval:number;
	private interval:number;

	constructor(props:AnimatorProps<T, U>){
		super();
		this.state = {
			playing: false,
			frame: 0,
			time: 0,
			percent: 0,
			timeline: props.timeline || [],
			data: props.timeline ? props.timeline[0].data : null
		};
		this.onTick = this.onTick.bind(this);
	}
	componentDidMount():void{
		if(this.props.startOnMount) this.start();
	}
	/*componentDidUpdate(pp:AnimatorProps<T, U>, ps:AnimatorState<T, U>):void{
		if(this.props.updateWhenExternalChanged && this.state.external !== ps.external){
			this.start();
		}
	}*/
	render():React.ReactNode{
		if(this.props.hideWhenDisabled && !this.state.playing) return null;
		
		let R:JSX.Element = this.props.children.call(this, this.state);

		R.props.className = "anim " + (R.props.className || "");

		return R;
	}

	private onTick():void{
		let time:number = this.inState('time').add(1000 / Animator.fps);
		let keyFrame:KeyFrame<T>;
		let data:T;

		keyFrame = this.state.timeline[this.currentKFIndex];
		if(time >= this.nextKFInterval){
			if(keyFrame.onExit) keyFrame.onExit();
			keyFrame = this.state.timeline[++this.currentKFIndex];
			this.nextKFInterval = this.getNextKFInterval();
			if(keyFrame.onEnter) keyFrame.onEnter();
		}
		if(this.nextKFInterval === Infinity){
			data = keyFrame.data;
		}else{
			data = keyFrame.lerp(this.state.timeline[this.currentKFIndex + 1], time);
		}
		if(time >= this.interval){
			time = this.inState('time').set(this.interval);
			if(this.props.onFinish) this.props.onFinish();
			this.stop();
		}
		this.inState('frame').add(1);
		this.inState('percent').set(time / this.interval);
		this.inState('data').set(data);
	}
	private getNextKFInterval():number{
		if(this.state.timeline.length <= this.currentKFIndex + 1) return Infinity;
		return this.state.timeline[this.currentKFIndex + 1].time;
	}

	/**
	 * 타임라인에 나열된 키 프레임을 하나 골라 주어진 키 프레임으로 대체한다.
	 * 
	 * @param index 키 프레임 번호
	 * @param value 새 키 프레임
	 */
	public setKeyFrame(index:number, value:KeyFrame<T>):Promise<void>{
		return this.inState('timeline').splice(index, 1, value);
	}
	/**
	 * 애니메이션을 시작 지점부터 재생한다.
	 */
	public start():void{
		let keyFrame:KeyFrame<T> = this.state.timeline[this.currentKFIndex = 0];

		if(keyFrame.onEnter) keyFrame.onEnter();
		this.nextKFInterval = this.getNextKFInterval();
		this.interval = this.state.timeline[this.state.timeline.length - 1].time;
		if(this.timer === -1){
			this.timer = window.setInterval(this.onTick, 1000 / Animator.fps);
		}
		this.setState({
			playing: true,
			time: 0,
			frame: 0
		});
	}
	/**
	 * 애니메이션을 중단한다.
	 */
	public stop():void{
		this.inState('playing').set(false);
		window.clearInterval(this.timer);
		this.timer = -1;
	}
}
/**
 * CSS 속성만을 수정하는 애니메이터 클래스
 */
export class CSSAnimator<U = any> extends Animator<React.CSSProperties, U>{
	/**
	 * 현재 보간된 CSS 속성 정보로부터 원하는 속성만을 골라 재구성한 CSS 속성 객체를 반환한다.
	 * 
	 * @param keys 원하는 속성 배열
	 */
	public filter(...keys:(keyof React.CSSProperties)[]):React.CSSProperties{
		let R:React.CSSProperties = {};

		for(const v of keys) R[v] = this.state.data[v];

		return R;
	}
}