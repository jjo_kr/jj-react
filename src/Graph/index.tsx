import React = require("react");
import Format from "jj-format";
import JRComponent from "../index";

/**
 * 그래프 컴포넌트의 React 속성 자료 구조
 */
export type GraphProps = {
	/**
	 * 추가 클래스 이름
	 * 
	 * 그래프 컴포넌트에는 클래스 `jj-graph`가 기본적으로 붙고,
	 * 그 뒤에 이 속성의 값이 추가된다.
	 */
	'className'?: string,
	/**
	 * 그래프 값의 최소 수치
	 */
	'minimum'?: number,
	/**
	 * 그래프 값의 최대 수치
	 */
	'maximum'?: number,
	/**
	 * 그래프 값
	 */
	'value': number,
	/**
	 * 컴포넌트 위에 보일 힌트 문구를 반환하는 함수
	 */
	'hint'?: (min:number, v:number, max:number) => string,
	/**
	 * 너비 대신 높이로 값을 시각화할지 여부
	 */
	'isVertical'?: boolean
};
/**
 * 주어진 값을 막대 그래프로 표현하는 컴포넌트
 */
export class Graph extends JRComponent<GraphProps>{
	render():JSX.Element{
		let rate:string = Format.ofRate(
			this.props.value - (this.props.minimum || 0),
			(this.props.maximum || 100) - (this.props.minimum || 0)
		);
		let hint:JSX.Element = this.props.hint
			? <div className="jj-graph-hint">{this.props.hint(
				this.props.minimum,
				this.props.value,
				this.props.maximum
			)}</div>
			: null
		;
		return <div className={"jj-graph " + (this.props.className || "")}>
			<div className="jj-graph-bar" style={{
				[this.props.isVertical ? 'height' : 'width']: rate
			}} />
			{hint}
			{this.props.children}
		</div>;
	}
}