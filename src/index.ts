import React = require("react");
import ReactStateUpdater = require("immutability-helper");

/**
 * 편리한 리액트 컴포넌트 클래스
 * 
 * 다루기 어려운 상태(state) 객체를 보다 쉽게 수정할 수 있도록 설계되었다.
 * 
 * @template P 리액트 속성의 자료형
 * @template S 리액트 상태의 자료형
 */
export default class JRComponent<P = {}, S = {}> extends React.Component<P, S>{
	protected constructor(props?:P){
		super(props);
		this.state = {} as any;
	}
	/**
	 * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
	 * 
	 * @param key 상태 식별자
	 */
	public inState<K extends keyof S>(key:K):JRState<P, S, K>{
		return new JRState(this, key);
	}
}
/**
 * 편리한 리액트 순수 컴포넌트 클래스
 * 
 * @template P 리액트 속성의 자료형
 * @template S 리액트 상태의 자료형
 */
export class JRPureComponent<P = {}, S = {}> extends React.PureComponent<P, S>{
	protected constructor(props?:P){
		super(props);
		this.state = {} as any;
	}
	/**
	 * 상태 객체로부터 주어진 식별자에 대응되는 값을 얻고, 이를 수정할 수 있는 내부 객체를 반환한다.
	 * 
	 * @param key 상태 식별자
	 */
	public inState<K extends keyof S>(key:K):JRState<P, S, K>{
		return new JRState(this, key);
	}
}
/**
 * 리액트 컴포넌트의 상태 객체의 한 값을 다루는 클래스
 * 
 * 메소드 {@link JRComponent#inState}로부터 인스턴스를 얻어 상태를 수정할 수 있도록 한다.
 */
export class JRState<P, S, K extends keyof S>{
	constructor(private caller:JRComponent<P, S>, private key:keyof S){}
	/**
	 * 대상 값에 주어진 값을 추가시키고 그 결과를 반환한다.
	 * 
	 * 대상 값은 숫자 또는 문자열이어야 한다.
	 * 
	 * @param delta 증가량. 다루는 값이 숫자인 경우 이 값을 수치로써 더하고, 문자열인 경우 이 값을 가장 끝에 더한다.
	 */
	public add(delta:S[K]):S[K]{
		let origin:S[K] = this.caller.state[this.key];
		let result:any;

		if(typeof origin === "number") result = { [this.key]: origin + Number(delta) };
		else if(typeof origin === "string") result = { [this.key]: origin + String(delta) };
		else throw Error(`Invalid state update: ${delta} onto the key ${this.key}`);

		this.caller.setState(result);

		return result[this.key];
	}
	/**
	 * 대상 값에 주어진 값을 가장 끝에 추가시킨다.
	 * 
	 * 대상 값은 배열이어야 한다.
	 * 
	 * @param items 추가 항목
	 */
	public push<T>(...items:T[]):Promise<void>{
		return new Promise((res, rej) => {
			this.caller.setState((prev:S, p:P):any => {
				let origin:S[K] = (prev as any)[this.key];

				if(!(origin instanceof Array)) throw Error(`Invalid state update: ${items} onto the key ${this.key}`);
				return {
					[this.key]: ReactStateUpdater(origin, {
						$push: items
					})
				};
			}, res);
		});
	}
	/**
	 * 대상 값을 주어진 인자의 내용에 맞도록 삭제 및 추가시킨다.
	 * 
	 * 대상 값은 배열이어야 한다.
	 * 
	 * @param index 축이 되는 배열의 인덱스
	 * @param deletes 축에서부터 삭제시킬 항목의 수
	 * @param items 삭제 후 추가시킬 항목
	 */
	public splice<T>(index:number, deletes:number, ...items:T[]):Promise<void>{
		return new Promise((res, rej) => {
			this.caller.setState((prev:S, p:P):any => {
				let origin:S[K] = (prev as any)[this.key];

				if(!(origin instanceof Array)) throw Error(`Invalid state update: ${items} onto the key ${this.key}`);
				return {
					[this.key]: ReactStateUpdater(origin, {
						$splice: [ [ index, deletes, ...items ] ]
					})
				};
			}, res);
		});
	}
	/**
	 * 대상 값을 주어진 값으로 대체하고 그 결과를 반환한다.
	 * 
	 * @param value 새로운 값
	 */
	public set(value:S[K]):S[K]{
		this.caller.setState((prev:S, p:P):any => ({
			[this.key]: value
		}));
		return value;
	}
}