import React from "react";
import { Animator, KeyFrame } from "../src/Animator";
import JRComponent from "../src/index";

let TestClass = Animator.createCustomAnimator<number>();

class Test extends JRComponent{
	render():JSX.Element{
		return <TestClass timeline={[
			new KeyFrame(0, 123)
		]}>{state => <div>
			
		</div>}</TestClass>;
	}
}